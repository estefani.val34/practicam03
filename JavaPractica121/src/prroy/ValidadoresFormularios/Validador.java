/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ValidadoresFormularios;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author roy
 */
public class Validador {
    static final int MAXDATA = 40;
    static final int MAXCOMMENT = 140;

    public Validador() {
    }
    
    
    public boolean ValidarCampoVacio(String name){
        return name != null && name.isEmpty();
    }
    
    public boolean ValidarMaxCaractersData(String name){
        return MAXDATA>name.length();
    }
    
    public boolean ValidarMaxComment(String comment){
        return MAXCOMMENT>comment.length();
    }
    
    static List<String> palabrasOfensivas = 
            new ArrayList<String>();
    
    public boolean ValidarTacos(String comment){
        boolean flag=true;
        String taco[] = {"ximple", "imbècil", "babau", "inútil", "burro", "loser", "noob", "capsigrany", "torrecollons",
        "fatxa", "nazi", "supremacista"};
        Collections.addAll(palabrasOfensivas, taco);
        
        for (String tacos : taco) {
            if(palabrasOfensivas.contains(taco)){
                flag=false;
                System.out.println("El texto no contiene ninguna de las palabras de la lista");
            }
        }
        
        return flag;
        
    }
    
    public boolean ValidarNombre(String name){
            String regex = "^[A-Z]+[a-z]*";
            return name.matches(regex);
    }
    
    public boolean ValidarEmail(String email){
            String regex = "^[\\w-+]+(\\.[\\w-]{1,62}){0,126}@[\\w-]{1,63}(\\.[\\w-]{1,62})+/[\\w-]+$";
            return email.matches(regex);
    }
    
    public boolean ValidarWeb(String website){
            String regex = "^(https?:\\/\\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?$";
            return website.matches(regex);
    }
    
}
