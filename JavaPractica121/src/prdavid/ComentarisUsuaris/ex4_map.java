/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComentarisUsuaris;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * contains the ex4 with map
 * @author medel
 */
public class ex4_map {

    private static Map<String, Usuari> people = new HashMap<String, Usuari>();

/**
 * crearPersonas()
 * method that create persons
 */
    private static void crearPersones() {
        people.put("Laura", new Usuari("Laura", "Laura@gmail.com", "www.laurawebsite.com"));
        people.put("Andrea", new Usuari("Andrea", "Andrea@gmail.com", "www.andreawebsite.com"));
        people.put("Dani", new Usuari("Dani", "Dani@gmail.com", "www.daniwebsite.com"));
        people.put("Pau", new Usuari("Pau", "Pau@gmail.com", "www.pauwebsite.com"));
        addComentsUser1(people.get("Laura"));
        addComentsUser2(people.get("Andrea"));
        addComentsUser3(people.get("Dani"));
    }

    /**
     * addComentsUser1
     * method that add coments to one user
     * @param user 
     */
    private static void addComentsUser1(Usuari user) {
        ArrayList<String> coments = new ArrayList<>();
        coments.add("Esto es el primer comentario del usuario Laura");
        coments.add("Esto es el segundo comentario del usuario Laura");
        coments.add("Esto es el tercer comentario del usuario Laura");
        coments.add("Esto es el cuarto y ultimo comentario del usuario Laura");
        user.setComentari(coments);
    }

    /**
     * addComentsUser2
     * method that add coments to one user
     * @param user 
     */
    private static void addComentsUser2(Usuari user) {
        ArrayList<String> coments = new ArrayList<>();
        coments.add("Esto es el primer comentario del usuario Andrea");
        coments.add("Esto es el segundo comentario del usuario Andrea");
        coments.add("Esto es el tercer comentario del usuario Andrea");
        coments.add("Esto es el cuarto y ultimo comentario del usuario Andrea");
        user.setComentari(coments);
    }

    /**
     * addComentsUser3
     * method that add coments to one user
     * @param user 
     */
    private static void addComentsUser3(Usuari user) {
        ArrayList<String> coments = new ArrayList<>();
        coments.add("Esto es el primer comentario del usuario Dani");
        coments.add("Esto es el segundo comentario del usuario Dani");
        coments.add("Esto es el tercer comentario del usuario Dani");
        coments.add("Esto es el cuarto y ultimo comentario del usuario Dani");
        user.setComentari(coments);
    }

    // https://stackoverflow.com/questions/5920135/printing-hashmap-in-java
    /**
     * mostrarPersonas()
     * method that shows all the users
     */
    private static void mostrarPersones() {
        for (String nombrePersona : people.keySet()) {
            String clave = nombrePersona;
            Usuari persona = people.get(clave);
            System.out.println(clave + " " + persona);
        }
    }

    /**
     * mostraUnComentari
     * method that shows one random comment
     * @param u 
     */
    private static void mostraUnComentari(Usuari u) {
        int rand = (int) Math.floor(Math.random() * 4);
        System.out.println(u.getComentari().get(rand));
    }

    /**
     * method that shows all the coments of the user
     * @param u 
     */
    private static void mostrarComentaris(Usuari u) {
        for (int i = 0; i < u.getComentari().size(); i++) {
            System.out.println(u.getComentari().get(i));
        }
    }

    /**
     * main
     * calls all the functions
     * @param args 
     */
    public static void main(String[] args) {
        crearPersones();
        mostrarPersones();
        System.out.println("\n mostra un comentari aleatori de l'usuari Laura:");
        mostraUnComentari(people.get("Laura"));
        System.out.println("\n mostra tots els comentaris de l'usuari Laura");
        mostrarComentaris(people.get("Laura"));
    }
}
