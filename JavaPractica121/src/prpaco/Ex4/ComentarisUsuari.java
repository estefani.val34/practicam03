/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex4;
import Ex3.Validador;
/**
 *
 * @author Francisco Regaña Corral
 */
public class ComentarisUsuari {
    
    //Atributs:
    private String nom;
    private String email;
    private String web; 
    private String comentari;
    
    
    
    //Getters & Setters...
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getComentari() {
        return comentari;
    }

    public void setComentari(String comentari) {
        this.comentari = comentari;
    }
    
    //Constructors:

    public ComentarisUsuari() {
    }
    
    
    public ComentarisUsuari(String nom, String email, String web, String comentari) {
        this.nom = nom;
        this.email = email;
        this.web = web;
        this.comentari = comentari;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();    
        sb.append("Nom: '");
        sb.append(this.nom);
        sb.append("' Email: '");
        sb.append(this.email);
        sb.append("' Web: '");
        sb.append(this.web);
        sb.append("' Comentari: '");
        sb.append(this.comentari);
        sb.append("'");
  
        
        return sb.toString();
    }

}
