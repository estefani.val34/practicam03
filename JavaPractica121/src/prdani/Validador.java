/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1.pkg2.m15;

import java.net.URL;

/**
 *
 * @author Daniel
 */

public class Validador {
    
    /**
     *
     * Comprobar si la URL @param website es válida.
     * @param website
     * @return devuelve "True" o "False".
     */
    public static boolean isValid(String website){
        try {
            new URL(website).toURI();
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }
    
    /**
     * Validator comprueba que no hayan errores.
     * 
     * @param nombre Nombre agregado en comentario.
     * @param email Email agregado en comentario.
     * @param website Página web agregada en comentario.
     * @param comment Comentario.
     * @return devuelve 1 si hay errores, en caso contrario 0.
     */
    public static int validator(String nombre, String email, String website, String comment) {

        int error = 0;
        int tmp_email_length = email.length(); 
        int user_name = 0;
        int serv_email = 0;
        int domain_email = 0;
        int at_email = 0;
        int dot_email = 0;
        
        String[] bad_words = {"ximple","imbècil","babau","inútil","burro","loser","noob","capsigrany","torrecollons","fatxa","nazi","supremacista"};

        if (nombre.length() > 40 || email.length() > 40 || website.length() > 40 || comment.length() > 140) {
            if (nombre.length() > 40) {
                System.out.println("ERROR! El nombre supera los 40 caracteres.");
                error = 1;
            }
            if (email.length() > 40) {
                System.out.println("ERROR! El e-mail supera los 40 caracteres.");
                error = 1;
            }
            if (website.length() > 40) {
                System.out.println("ERROR! La página web supera los 40 caracteres.");
                error = 1;
            }
            if (comment.length() > 140) {
                System.out.println("ERROR! El comentario supera los 140 caracteres.");
                error = 1;
            }
        }
        if (nombre.isEmpty() || nombre.charAt(0) < 34 || nombre.charAt(0) > 90) {
            if (nombre.isEmpty()) {
                System.out.println("ERROR! El nombre está vacío.");
                error = 1;
            } else if (nombre.charAt(0) < 34 || nombre.charAt(0) > 90) {
                System.out.println("ERROR! El nombre no comienza por mayúsculas.");
                error = 1;
            }
        }
        for (int i = 0; i <= tmp_email_length - 1; i++) {
            char email_letters = email.charAt(i);
            if (email_letters == '@' || email_letters == '.') { // <-- "@"
                if (email_letters == '@') {
                    at_email++;
                } else if (at_email == 1 && email_letters == '.') {
                    dot_email = 1;
                }
            } else {
                if (at_email == 0) {
                    user_name++;
                } else if (at_email == 1 && dot_email == 0) {
                    serv_email++;
                } else if (at_email == 1 && dot_email == 1) {
                    domain_email++;
                }
            }
        }

        if (email.isEmpty()) {
            System.out.println("ERROR! El e-mail está vacío.");
            error = 1;
        } else if (at_email != 1 || dot_email != 1 || user_name <= 1 || serv_email <= 1 || domain_email <= 1) {
            System.out.println("ERROR! El e-mail no tiene un formato válido.");
            error = 1;
        }
        
        if (!website.isEmpty()){
            if (!isValid(website)){
            System.out.println("ERROR! La página web no es válida.");
            error = 1;
            } 
        }
        
        
        for(int i = 0; i < bad_words.length; i++){
            if (comment.contains(bad_words[i])){
                System.out.println("ERROR! El comentario contiene palabras prohibidas.");
                error = 1;
            }
        }
        
        return error;

    }
}


