/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1.pkg2.m15;

/**
 *
 * @author Daniel
 */
public class PruebaValidador {

    public static void datos(String nombre, String email, String website, String comment) {

        //Datos correctos:
        
        System.out.println("Nombre: ");
        nombre = "Paco";
        System.out.println("> " + nombre);
        
        System.out.println("E-mail: ");
        email = "aaa@hotmail.com";
        System.out.println("> " + email);
        
        System.out.println("Página web: ");
        website = "https://www.google.es";
        System.out.println("> " + website);
        
        System.out.println("Comentario: ");
        comment = "Me encanta esta página web.";
        System.out.println("> " + comment);
        
        System.out.print("\n");

        if (Validador.validator(nombre, email, website, comment) != 1) {
            System.out.println("Datos correctos.");
        } else {
            System.out.println("Tu comentario no cumple los requisitos para ser publicado.");
        }
        
        System.out.print("\n");
        
        //Datos incorrectos:
        
        System.out.println("Nombre: ");
        nombre = "alex";
        System.out.println("> " + nombre);
        
        System.out.println("E-mail: ");
        email = "aaa@h@otmail.com";
        System.out.println("> " + email);
        
        System.out.println("Página web: ");
        website = ".goog.le.es";
        System.out.println("> " + website);
        
        System.out.println("Comentario: ");
        comment = "¡Eres un noob!";
        System.out.println("> " + comment);
        
        System.out.print("\n");

        if (Validador.validator(nombre, email, website, comment) != 1) {
            System.out.println("Datos correctos.");
        } else {
            System.out.println("Tu comentario no cumple los requisitos para ser publicado.");
        }
    }
}
