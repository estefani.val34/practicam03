/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1.pkg2.m15;

import java.util.Scanner;

/**
 *
 * @author tarda
 */
public class Practica12M15 {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {

        String nombre = "";
        String email = "";
        String website = "";
        String comment = "";

        
        
        Scanner teclado = new Scanner(System.in);
        System.out.println("Nombre: ");
        nombre = teclado.nextLine();
        System.out.println("E-mail: ");
        email = teclado.nextLine();
        System.out.println("Página web: ");
        website = teclado.nextLine();
        System.out.println("Comentario: ");
        comment = teclado.nextLine();
        
        System.out.print("\n");

        if(Validador.validator(nombre, email, website, comment) != 1){
            System.out.println("Datos correctos.");
        } else {
            System.out.println("Tu comentario no cumple los requisitos para ser publicado.");
        }

        System.out.print("\n");
        
        PruebaValidador.datos(nombre, email, website, comment);
    }

}
