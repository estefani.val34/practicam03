package Ejercicio2;

public class Exercici2 {

    public static void main(String[] args) {

        /**
         * Aquesta clase crea dos pokemons, un el cual li paso tots els atributs
         * declarats en la clase Pokemon, i l'altre només té tres atributs
         */
        Pokemon pokemon1 = new Pokemon("123", "Pikachu", "Electric","Fire", 22, 200, 300, 20,20,50,60,60);
        Pokemon pokemon2 = new Pokemon("456","Charmander","Fire");
        
        System.out.println(pokemon1);
        System.out.println(pokemon2);
        
        
        /**
         * Pregunta: Si una propietat té la visibilitat private; és necessari crear un mètode 
         * per a poder-hi accedir o es pot accedir dirèctament a veure/editar la propietat de 
         * la següent manera: pokemon.tipo1 Per què?
         * Per accedir a una propietat que té visibilitat privada cal crear un mètode Get,
         * que ens tornarà la variable tipo1.
         */
        
        System.out.println("Tipo Pokemon1: " + pokemon1.getTipo1());


    }

}
