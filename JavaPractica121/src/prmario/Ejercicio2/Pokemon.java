package Ejercicio2;

public class Pokemon {

    private String Pok_id;
    private String nombre;
    private String tipo1;
    private String tipo2;
    private int nivel;
    private int energiaAct;
    private int energiaMax;
    private double ataque;
    private double defensa;
    private double velocidad;
    private double defensaEsp;
    private double ataqueEsp;

    
    
    /**
     * CONSTRUCTORS
     */
    /**
     * Default Constructor
     */
    public Pokemon() {

    }
    
    /**
     * 
     * @param Pok_id
     * @param nombre
     * @param tipo1
     * @param tipo2
     * @param nivel
     * @param energiaAct
     * @param energiaMax
     * @param ataque
     * @param defensa
     * @param velocidad
     * @param defensaEsp
     * @param ataqueEsp 
     */
    public Pokemon(String Pok_id, String nombre, String tipo1, String tipo2, int nivel, int energiaAct, int energiaMax, double ataque, double defensa, double velocidad, double defensaEsp, double ataqueEsp) {
        this.Pok_id = Pok_id;
        this.nombre = nombre;
        this.tipo1 = tipo1;
        this.tipo2 = tipo2;
        this.nivel = nivel;
        this.energiaAct = energiaAct;
        this.energiaMax = energiaMax;
        this.ataque = ataque;
        this.defensa = defensa;
        this.velocidad = velocidad;
        this.defensaEsp = defensaEsp;
        this.ataqueEsp = ataqueEsp;
    }
    
    
    /**
     * 
     * @param Pok_id
     * @param nombre
     * @param tipo1 
     */
    public Pokemon(String Pok_id, String nombre, String tipo1) {
        this.Pok_id = Pok_id;
        this.nombre = nombre;
        this.tipo1 = tipo1;
    }

    /**
     * GETTERS AND SETTERS
     */
    
    /**
     * @return Pok_id
     */
    public String getPok_id() {
        return Pok_id;
    }

    /**
     * 
     * @param Pok_id 
     */
    public void setPok_id(String Pok_id) {
        this.Pok_id = Pok_id;
    }

    /**
     * 
     * @return nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * 
     * @param nombre 
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * 
     * @return tipo1
     */
    public String getTipo1() {
        return tipo1;
    }

    /**
     * 
     * @param tipo1 
     */
    public void setTipo1(String tipo1) {
        this.tipo1 = tipo1;
    }

    /**
     * 
     * @return tipo2 
     */
    public String getTipo2() {
        return tipo2;
    }

    /**
     * @param tipo2 
     */
    public void setTipo2(String tipo2) {
        this.tipo2 = tipo2;
    }

    /**
     * 
     * @return nivel 
     */
    public int getNivel() {
        return nivel;
    }

    /**
     * 
     * @param nivel 
     */
    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    /**
     * 
     * @return energiaAct
     */
    public int getEnergiaAct() {
        return energiaAct;
    }

    /**
     * 
     * @param energiaAct 
     */
    public void setEnergiaAct(int energiaAct) {
        this.energiaAct = energiaAct;
    }

    /**
     * 
     * @return energiaMax
     */
    public int getEnergiaMax() {
        return energiaMax;
    }

    /**
     * 
     * @param energiaMax 
     */
    public void setEnergiaMax(int energiaMax) {
        this.energiaMax = energiaMax;
    }

    /**
     * 
     * @return ataque
     */
    public double getAtaque() {
        return ataque;
    }

    /**
     * 
     * @param ataque 
     */
    public void setAtaque(double ataque) {
        this.ataque = ataque;
    }

    /**
     * 
     * @return defensa
     */
    public double getDefensa() {
        return defensa;
    }

    /**
     * 
     * @param defensa 
     */
    public void setDefensa(double defensa) {
        this.defensa = defensa;
    }

    /**
     * 
     * @return velocidad
     */
    public double getVelocidad() {
        return velocidad;
    }

    /**
     * 
     * @param velocidad 
     */
    public void setVelocidad(double velocidad) {
        this.velocidad = velocidad;
    }

    /**
     * 
     * @return defensaEsp
     */
    public double getDefensaEsp() {
        return defensaEsp;
    }

    /**
     * 
     * @param defensaEsp 
     */
    public void setDefensaEsp(double defensaEsp) {
        this.defensaEsp = defensaEsp;
    }

    /**
     * 
     * @return ataqueEsp
     */
    public double getAtaqueEsp() {
        return ataqueEsp;
    }

    /**
     * 
     * @param ataqueEsp 
     */
    public void setAtaqueEsp(double ataqueEsp) {
        this.ataqueEsp = ataqueEsp;
    }

    /**
     * Funció que imprimeix tots els atributs de Pokemon 
     */
    @Override
    public String toString() {
        return "Pokemon{" + "Pok_id=" + Pok_id + ", nombre=" + nombre + ", tipo1=" + tipo1 + ", tipo2=" + tipo2 + ", nivel=" + nivel + ", energiaAct=" + energiaAct + ", energiaMax=" + energiaMax + ", ataque=" + ataque + ", defensa=" + defensa + ", velocidad=" + velocidad + ", defensaEsp=" + defensaEsp + ", ataqueEsp=" + ataqueEsp + '}';
    }

}
