package Ejercicio4;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class ejercicio04 {

    public static void main(String[] args) {

        ComentarisUsuari[] comentariList = new ComentarisUsuari[4];
         //@see Se crea idividualmente los usuarios untroduciendo por usuario
        
        comentariList[0] = new ComentarisUsuari("Mario", "Mario@mail.com", "Mario.com",
                "My name is Mario and i'm 19");
        comentariList[1] = new ComentarisUsuari("Pepe", "Pepe@mail.com", "Pepe.com",
                "My name is Pepe and i'm 65");
        comentariList[2] = new ComentarisUsuari("Raul", "Raul@mail.com", "Raul.com",
                "My name is Raul and i'm 20");
        comentariList[3] = new ComentarisUsuari("Carlos", "Carlos@mail.com", "Carlos.com",
                "My name is Carlos and i'm 3");

        //@see Se pide al usuario introducir el nombre ya registrado
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce el nombre  de usuario: ");
        String name = sc.nextLine();
        
        //@see Se comprara con el nombre introducido para que busque en el string el nom e imprima los comentarios
        for (ComentarisUsuari comentariaUsuari : comentariList) {
            if (name.equals(comentariaUsuari.getNom())) {
                System.out.println(comentariaUsuari);
            }
        }
        //@see Imprime todos los comentarios
        System.out.println("\n");
        for (ComentarisUsuari comentariaUsuari : comentariList) {
            System.out.println(comentariaUsuari.getComentari());
        }
        //@see HashMap que crea la key como nombre y value los comentarios
        System.out.println("\n");
        Map<String, String> map = new HashMap<>();
        map.put("Mario", "My name is Mario and i'm 19");
        map.put("Pepe", "My name is Pepe and i'm 65");
        map.put("Raul", "My name is Raul and i'm 20");
        map.put("Carlos", "My name is Carlos and i'm 3");
                
        //@see Bucle que imprime todas las keys y values del HashMap
        Iterator<String> it = map.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next();
            System.out.println("Clave: " + key + " -> Valor: " + map.get(key));
        }
        
    }

    

}
