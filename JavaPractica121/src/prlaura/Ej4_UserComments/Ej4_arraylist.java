
package prlaura.Ej4_UserComments;

import java.util.*;

public class Ej4_arraylist {
    
    private static final ArrayList<User> USERS = new ArrayList<>();
        
    /**
     * This functions creates new users with their comments
     * @return arraylist users
     */
    private static ArrayList<User> addUsers(){
        ArrayList<String> commentsLaura = new ArrayList<>();
        ArrayList<String> commentsDiego = new ArrayList<>();
        
        String comment1Laura = "Hola, me llamo Laura";
        String comment2Laura = "Tengo 29 años";
        String comment3Laura = "Vivo en Barcelona";
        String comment1Diego = "Hola, me llamo Diego";
        
        commentsLaura.add(comment1Laura);
        commentsLaura.add(comment2Laura);
        commentsLaura.add(comment3Laura);
        commentsDiego.add(comment1Diego);
        
        User laura = new User("Laura","laura@gmail.com","www.laura.com",commentsLaura);
        User diego = new User("Diego","diego@gmail.com","www.diego.com",commentsDiego);
        
        USERS.add(laura);
        USERS.add(diego);
        
        return USERS;
    }
    
    /**
     * This function prints the comments by one user
     */
    private static void showCommentsByUser(){
        Scanner myScan = new Scanner(System.in);
        System.out.print("Introduce a name: ");
        String nameIntro = myScan.nextLine();
        System.out.println("Comments by "+nameIntro);
        for (User user : USERS) {
            if (user.getName().equals(nameIntro)){
                System.out.println(user.getComments());
            }
        }
    }
    
    /**
     * This function prints all comments by all users
     */
    private static void showAllComments(){
        System.out.println("\nAll comments:");
        for (User user : USERS) {
            System.out.println(user.getComments());
        }
    }
    
    /**
     * This function runs the different functions
     * @param args 
     */
    public static void main(String[] args) {
        ArrayList<User> users = addUsers();
        showCommentsByUser();
        showAllComments();
    }
}