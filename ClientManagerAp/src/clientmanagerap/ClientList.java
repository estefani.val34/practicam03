/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientmanagerap;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author tarda
 */
public class ClientList {

    private Set<Client> myClients = new HashSet<>();

    //constructor
    public ClientList() {
    }

    //getters +setters
    public Set<Client> getMyClients() {
        return myClients;
    }

    public void setMyClients(Set<Client> myClients) {
        this.myClients = myClients;
    }

    /**
     * findAClientById() Ask the user for a client id and searches for him in
     * the list.
     */
    public void findAClientById() {
        // TODO
        System.out.println("Searching client...");

    }

    /**
     * listAllClients() Shows the list of clients.
     */
    public void listAllClients() {
        System.out.println("Listing all clients...");
        //TODO
    }

    /**
     * addANewClient() Adds a new client to the list. First, it asks for the new
     * client data to the user, creates a new client with that data, and add the
     * new client to the list.
     */
    public void addANewClient() {
        System.out.println("Adding a new client...");

        //TODO
    }

    /**
     * modifyAClient() Modifies a client. First, it asks the client id, then, it
     * look him up in the list. If found, it shows the actual client information
     * and ask for new data. Finally, it modifies the client. If not found, it
     * reports the error to the user.
     */
    public void modifyAClient() {
        // TODO
        System.out.println("Modifying a client...");
    }

    /**
     * removeAClient() Removes a client. First, it asks the client id, then, it
     * look him up in the list. If found, it shows the actual client information
     * and ask for confirmation. After comfirmed, it removes the client. If not
     * found, it reports the error to the user.
     */
    public void removeAClient() {
        // TODO
        System.out.println("Removing a client...");
    }

    /**
     * readClient() Read from user a client data.
     *
     * @return client object with inputted data or null if an error occurs.
     */
    private Client readClient() {
        // TODO
        System.out.println("Reading client data...");
        return null;
        //return new Client();
    }

    /**
     * loadClients() Loads initial data into client list.
     */
    public void loadClients() {
        System.out.println("Loading client data into list of clients...");
        myClients.add(new Client("001A", "Peter", "93001", "Addr01", 1001.0));
        myClients.add(new Client("001B", "Paul", "93002", "Addr02", 1002.0));
        myClients.add(new Client("001C", "Mary", "93003", "Addr03", 1003.0));
        myClients.add(new Client("001D", "Bob", "93004", "Addr04", 1004.0));
        myClients.add(new Client("001E", "Sophie", "93005", "Addr05", 1005.0));
        myClients.add(new Client("001F", "Andrew", "93006", "Addr06", 1006.0));
        myClients.add(new Client("001G", "Phil", "93007", "Addr07", 1007.0));
    }

}
